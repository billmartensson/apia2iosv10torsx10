//
//  ViewController.swift
//  apia2iosv10torsX10
//
//  Created by Bill Martensson on 2018-06-07.
//  Copyright � 2018 Magic Technology. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var theLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func doStuff(_ sender: Any) {
        if(theLabel.text != "Gurka!!")
        {
            theLabel.text = "Gurka!!"
        } else {
            theLabel.text = "Kiwi!!"
        }
        
    }
    
}

